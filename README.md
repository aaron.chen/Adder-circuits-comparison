# adder-circuits-comparison
(implementation of Lab 4 for ECE 385)
### By Aaron Chen, Shruti Chanumolu

8-bit Carry-lookahead, Carry-select, and Ripple Adders implemented in systemverilog for testing / comparison

We designed an 8-bit processor and implemented three types of adders: carry-ripple adder, a carry-lookahead adder, and a carry-select adder. This helped us understand the basic syntax for SystemVerilog and attain the basics required to operate Quartus II, a CAD tool for FPGA synthesis and simulation.

lab4_adders_toplevel.sv should be set as top level. The different types of adders within lab4_adders_toplevel can be commented / uncommented out to use different adders.

### Ripple Adder
<img src="/ripple_adder_block.png" alt="ripple_adder_block" height="300" />

### Carry-Select Adder
![carry_select_adder_block](/carry_select_adder_block.png)

### Carry-Lookahead Adder
![carry_lookahead_block](/carry_lookahead_block.png)
