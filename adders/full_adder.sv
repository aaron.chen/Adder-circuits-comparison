//full adder module (add two bits, gen carry)
module full_adder
(
	input logic a,b, // operand bits to be added
	input logic cin, // carry in
	output logic cout, // carry out
	output logic s // sum-bit
);

logic xor_3bit_out;

xor_3bit xor_3bit_unit
(
	.a(a),
	.b(b),
	.c(cin),
	.out(xor_3bit_out)
);

assign s = xor_3bit_out; // s == a,b,c all XORed
assign cout = (a & b) | (a & cin) | (b & cin); // carry if any two are high
	 
endmodule : full_adder
