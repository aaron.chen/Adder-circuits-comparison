//4 bit carry select adder (using 4-bit ripple adder)
module carry_select_adder_4bit
(
    input   logic[3:0]     a,
    input   logic[3:0]     b,
    input   logic          cin, // cin
    output  logic[3:0]     s, //sum
    output  logic          cout // carry out
);

//sum of 4-bit adder has carry=0 and 1 resp.
logic [3:0] sum_adder0;
logic [3:0] sum_adder1;
//carry of 4-bit adder has carry=0 and 1 resp.
logic car_adder0;
logic car_adder1;

//adder no carry
ripple_adder_4bit adder0
(
	.a(a),
	.b(b),
	.cin(1'b0),
	.s(sum_adder0),
	.cout(car_adder0)
);

//adder w/ carry
ripple_adder_4bit adder1
(
	.a(a),
	.b(b),
	.cin(1'b1),
	.s(sum_adder1),
	.cout(car_adder1)
);

//mux between two sums when carry comes in.
mux2to1 #(.width(4)) summux
(
	.sel(cin),
	.a(sum_adder0), .b(sum_adder1),
	.out(s)
);

assign cout = car_adder0 | (car_adder1 & cin); // either cout is produced even without a cin, or cin is needed AND cin comes in 

endmodule : carry_select_adder_4bit
