module carry_lookahead_adder_4bit
(
	input   logic[3:0]     a,
	input   logic[3:0]     b,
	input   logic          cin,
	output  logic[3:0]     s, //sum
	output  logic          cout,
	output  logic          pg, //group P function
	output  logic          gg //group G function
);

logic p_arr [3:0]; //internal logic to represent P (xor) from each adder
logic g_arr [3:0]; //internal logic to represent G (add) from each adder
logic cin_arr [3:0]; //internal logic to represent cin to each adder
logic cout_arr [3:0]; //internal logic to represent cout from each adder


//generate full adders that produce p and g (kinda redundant, but at least in parallel?), sum and carry
generate
	genvar i;
	for (i = 0; i < 4; i = i + 1) begin : fa_gen
		full_adder_pg fa_arr
		(
			.a(a[i]),
			.b(b[i]),
			.cin(cin_arr[i]),
			.cout(cout_arr[i]),
			.s(s[i]),
			.p(p_arr[i]),
			.g(g_arr[i])
		);
	end
endgenerate

//figure out carry ins
assign cin_arr[0] = cin; //c0 = G_-1
assign cin_arr[1] = (cin & (p_arr[0])) | (g_arr[0]); //c1 = G_−1*P0 + G0
//c2 = G_−1*P0*P1 + G0*P1 + G1
assign cin_arr[2] = (cin & (p_arr[0]) & (p_arr[1])) | ((g_arr[0]) & (p_arr[1])) | (g_arr[1]);
//c3 = G_−1*P0*P1*P2 + G0*P1*P2 + G1*P2 + G2
assign cin_arr[3] = (cin & (p_arr[0]) & (p_arr[1]) & (p_arr[2])) //G_−1*P0*P1*P2
			| ((g_arr[0]) & (p_arr[1]) & (p_arr[2])) //G0*P1*P2
			| ((g_arr[1]) & (p_arr[2])) //G1*P2
			| ((g_arr[2]));//G2


assign cout = cout_arr[3]; // cout is the last cout
assign pg = p_arr[0] & p_arr[1] & p_arr[2] & p_arr[3]; // PG = P0 * P1 * P2 * P3
assign gg = (g_arr[3])
			| (g_arr[2] & p_arr[3])
			| (g_arr[1] & p_arr[3] & p_arr[2])
			| (g_arr[0] & p_arr[3] & p_arr[2] & p_arr[1]); // G3 + G2*P3 + G1*P3*P2 + G0*P3*P2*P1
	 
endmodule : carry_lookahead_adder_4bit
