//2-1 mux (variable width), sel toggles between out a and b
module mux2to1 #(parameter width = 16)
(
	input logic sel,
	input logic [width-1:0] a, b,
	output logic [width-1:0] out
);

always_comb
begin
	if (sel == 0)
		out = a;
	else
		out = b;
end

endmodule : mux2to1