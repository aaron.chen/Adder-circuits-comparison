module testbench();

timeunit 10ns;	// Half clock cycle at 50 MHz
			// This is the amount of time represented by #1 
timeprecision 1ns;

// These signals are internal because the processor will be 
// instantiated as a submodule in testbench.
//inputs to circuits
logic           Clk;        // 50MHz clock is only used to get timing estimate data
logic           Reset;      // From push-button 0.  Remember the button is active low (0 when pressed)
logic           LoadB;      // From push-button 1
logic           Run;        // From push-button 3.
logic[15:0]     SW;         // From slider switches
    
//outputs from circuit
logic           CO;         // Carry-out.  Goes to the green LED to the left of the hex displays.
logic[15:0]     Sum;        // Goes to the red LEDs.  You need to press "Run" before the sum shows up here.
logic[6:0]      Ahex0;      // Hex drivers display both inputs to the adder.
logic[6:0]      Ahex1;
logic[6:0]      Ahex2;
logic[6:0]      Ahex3;
logic[6:0]      Bhex0;
logic[6:0]      Bhex1;
logic[6:0]      Bhex2;
logic[6:0]      Bhex3;

// test nums
logic[15:0] t1 = 16'h3a6e;
logic[15:0] t2 = 16'hfa1a;
logic[15:0] t3 = 16'h8df7;

integer successes = 0;
		
// Instantiating the DUT
lab4_adders_toplevel circuit(.*);	

// Toggle the clock
// #1 means wait for a delay of 1 timeunit
always begin : CLOCK_GENERATION
#1 Clk = ~Clk;
end

initial begin: CLOCK_INITIALIZATION
    Clk = 0;
end

// Testing begins here
// The initial block is not synthesizable
// Everything happens sequentially inside an initial block
// as in a software program
initial begin: TEST_VECTORS
//init
Reset = 0;		// reset it
LoadB = 1;		//don't load b, const load a
Run = 1;    // don't run yet

#2 Reset = 1; //stop reset
//TEST 1
#2 SW = t2;
#2 LoadB = 0;
#2 LoadB = 1;
#2 SW = t1;
// A = x3a6e
// B = xfa1a

#2 Run = 0; // get the sum
#22 Run = 1; // unswitch Run
$display("TEST 1");
$display("       A: %b", t1);
$display("       B: %b", t2);
$display("Calc Sum: %b", Sum);
$display("Real Sum: %b", t1+t2);
	if (Sum != (t1 + t2)) // Expected result of 1st cycle
	    $display("Test 1 failed!");
	else
		successes = successes + 1;

//TEST 2
#2 Run = 0;	// Toggle Run without doing anything
#2 Run = 1;
#22 Run = 0;
$display("TEST 2");
$display("       A: %b", t1);
$display("       B: %b", t2);
$display("Calc Sum: %b", Sum);
$display("Real Sum: %b", t1+t2);
	if (Sum != (t1 + t2)) // Expected result of 2nd cycle (stay same)
	    $display("Test 2 failed!");
	else
		successes = successes + 1;

//TEST 3
#2 SW = t3;
#2 LoadB = 0;
#2 LoadB = 1;
#2 SW = t2;
// A = xfa1a
// B = x8df7

#2 Run = 0; // get the sum
#22 Run = 1; // unswitch Run
$display("TEST 3");
$display("       A: %b", t2);
$display("       B: %b", t3);
$display("Calc Sum: %b", Sum);
$display("Real Sum: %b", t2+t3);
	if (Sum != (t2 + t3)) // Expected result of 1st cycle
	    $display("Test 3 failed!");
	else
		successes = successes + 1;

//TEST 4
#2 SW = t1; //(keep b the same)

#2 Run = 0; // get the sum
#200 Run = 1; // unswitch Run
$display("TEST 4");
$display("       A: %b", t1);
$display("       B: %b", t3);
$display("Calc Sum: %b", Sum);
$display("Real Sum: %b", t1+t3);
	if (Sum != (t1 + t3)) // Expected result of 1st cycle
	    $display("Test 4 failed!");
	else
		successes = successes + 1;

if (successes == 4)
	$display("SUCCESS!");
else
	$display("Something went wrong.");
end
endmodule
