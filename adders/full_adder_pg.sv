//full adder module (add two bits, no carry, generate sum, and p (xor) and g(and))
module full_adder_pg
(
	input logic a,b, // operand bits to be added
	input logic cin, // carry in
	output logic cout, // carry in
	output logic s, // sum-bit
	output logic p, // a XOR b
	output logic g // a AND b
);

logic xor_3bit_out;

xor_3bit xor_3bit_unit
(
	.a(a),
	.b(b),
	.c(cin),
	.out(xor_3bit_out)
);

assign s = xor_3bit_out; //s == a,b,c all XORed
assign p = a ^ b;
assign g = a & b;
assign cout = (a & b) | (a & cin) | (b & cin); // carry if any two are high
	 
endmodule : full_adder_pg
