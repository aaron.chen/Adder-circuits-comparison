//16 bit, no carry ins or p g generated (explicitly anyway)
//gives arguments to recursive 16-bit cla adder
module carry_lookahead_adder
(
	input   logic[15:0]     A,
	input   logic[15:0]     B,
	output  logic[15:0]     Sum, //sum
	output  logic           CO
);

logic pg_arr [3:0]; //internal logic to represent PG (P/xor group) from each cla
logic gg_arr [3:0]; //internal logic to represent GG (gen group) from each cla
logic cin_arr [3:0]; //internal logic to represent cin to each cla
logic cout_arr [3:0]; //internal logic to represent cout from each cla

//generate CLAs that produce p and g (kinda redundant, but at least in parallel?), sum and carry (groups of 4 bits)
generate
	genvar i;
	for (i = 0; i < 4; i = i + 1) begin : cla_gen
		carry_lookahead_adder_4bit cla_arr
		(
			.a(A[4*i+3:4*i]),
			.b(B[4*i+3:4*i]),
			.cin(cin_arr[i]),
			.cout(cout_arr[i]),
			.s(Sum[4*i+3:4*i]),
			.pg(pg_arr[i]),
			.gg(gg_arr[i])
		);
	end
endgenerate

//figure out carry ins
assign cin_arr[0] = 1'b0; //nothing in
assign cin_arr[1] = gg_arr[0]; //C4 = g0
assign cin_arr[2] = ((gg_arr[0]) & (pg_arr[1])) | gg_arr[1]; //c2 = G0*P1 + G1
//c3 = G0*P1*P2 + G1*P2 + G2
assign cin_arr[3] = ((gg_arr[0]) & (pg_arr[1]) & (pg_arr[2])) //G0*P1*P2
			| (gg_arr[1] & (pg_arr[2])) //G1*P2
			| (gg_arr[2]);//G2

assign CO = cout_arr[3]; // CO is the last cout
//don't need to calc pg,gg

endmodule : carry_lookahead_adder
